package ws

import (
	"encoding/json"
	"log"
	"strings"

	bybit_go "bitbucket.org/devgre/bybit-go"
	"bitbucket.org/devgre/bybit-go/perpetual/api"

	"github.com/tidwall/gjson"
)

const (
	PublicEndpoint  = "/realtime_public"
	PrivateEndpoint = "/realtime_private"
)

type ByBitWS struct {
	*bybit_go.BaseWS
	private *bybit_go.BaseWS

	DebugMode bool
}

func NewWS(config *bybit_go.Configuration) *ByBitWS {
	cfg := &bybit_go.Configuration{
		Addr:      config.Addr + PublicEndpoint,
		DebugMode: config.DebugMode,
	}

	b := &ByBitWS{
		DebugMode: cfg.DebugMode,
	}
	b.BaseWS = bybit_go.NewBaseWS(cfg)

	if config.ApiKey != "" && config.SecretKey != "" {
		cfg2 := &bybit_go.Configuration{
			Addr:      config.Addr + PrivateEndpoint,
			ApiKey:    config.ApiKey,
			SecretKey: config.SecretKey,
			DebugMode: config.DebugMode,
		}
		b.private = bybit_go.NewBaseWS(cfg2)
	}
	return b
}

func (b *ByBitWS) Connect() error {
	var err error
	if err = b.Start(nil, b.processMessage); err != nil {
		return err
	}

	if b.private != nil {
		if err = b.private.Start(nil, b.processMessage); err != nil {
			return err
		}
	}

	return nil
}

func (b *ByBitWS) Disconnect() {
	if b.private != nil {
		b.private.Stop()
	}
	b.Stop()
}

func (b *ByBitWS) Subscribe(topic string, isPrivate bool) error {
	cmd := Cmd{
		Op:   "subscribe",
		Args: []interface{}{topic},
	}

	if isPrivate {
		return b.private.Sub(cmd.toString())
	}

	return b.Sub(cmd.toString())
}

func (b *ByBitWS) Unsubscribe(topic string, isPrivate bool) error {
	cmd := Cmd{
		Op:   "unsubscribe",
		Args: []interface{}{topic},
	}

	if isPrivate {
		return b.private.Unsub(cmd.toString())
	}
	return b.Unsub(cmd.toString())
}

func (b *ByBitWS) processMessage(messageType int, data []byte) {
	ret := gjson.ParseBytes(data)

	if b.DebugMode {
		log.Printf("ByBitWS message: %v", string(data))
	}

	if ret.Get("ret_msg").String() == "pong" {
		//_ = b.HandlePong()
	}

	if topicValue := ret.Get("topic"); topicValue.Exists() {
		topic := topicValue.String()
		if strings.HasPrefix(topic, TopicKLine) {
			// candle.5.BTCUSDT
			topicArray := strings.Split(topic, ".")
			if len(topicArray) != 3 {
				return
			}
			symbol := topicArray[2]

			raw := ret.Get("data").Raw
			var data []KLine
			err := json.Unmarshal([]byte(raw), &data)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processKLine(symbol, data...)
		} else if topic == TopicOrder {
			var orderReport []api.Order

			raw := ret.Get("data").Raw
			err := json.Unmarshal([]byte(raw), &orderReport)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processOrder(orderReport)
		} else if topic == TopicExecution {
			var execReport []Execution

			raw := ret.Get("data").Raw
			err := json.Unmarshal([]byte(raw), &execReport)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processExecution(execReport)
		} else if topic == TopicPosition {
			var positionReport []api.Position

			raw := ret.Get("data").Raw
			err := json.Unmarshal([]byte(raw), &positionReport)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processPosition(positionReport)
		} else if topic == TopicWallet {
			var walletInfo []Wallet

			raw := ret.Get("data").Raw
			err := json.Unmarshal([]byte(raw), &walletInfo)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processWallet(walletInfo)
		}
	}
}
