package ws

type Subscription struct {
	Success bool   `json:"success"`
	RetMsg  string `json:"ret_msg"`
	ConnId  string `json:"conn_id"`
	Request Cmd    `json:"request"`
}

type KLine struct {
	Start    int64   `json:"start"` // 1640249700
	Open     float64 `json:"open"`
	High     float64 `json:"high"`
	Low      float64 `json:"low"`
	Close    float64 `json:"close"`
	Volume   string  `json:"volume"`
	Turnover string  `json:"turnover"` // 0.0013844
	Period   string  `json:"period"`   // 1m
	Confirm  bool    `json:"confirm"`  // If confirm is True, the data is the final tick for the interval. Otherwise, it is a snapshot.
}

type KLineResult struct {
	Topic string  `json:"topic"`
	Data  []KLine `json:"data"`
}

type Execution struct {
	Symbol       	string 	`json:"symbol"`
	Side         	string 	`json:"side"`   			// BUY SELL
	OrderId      	string 	`json:"order_id"`
	OrderLinkId  	string 	`json:"order_link_id"`
	ExecId			string	`json:"exec_id"`			// Transaction ID
	Price        	float64	`json:"price"`
	OrderQty      	float64 `json:"order_qty"`
	ExecType		string	`json:"exec_type"`
	ExecQty			float64	`json:"exec_qty"`			// Transaction qty
	ExecFee			float64	`json:"exec_fee"`			// Transaction fee
	LeavesQty		float64	`json:"leaves_qty"`
	IsMaker			bool	`json:"is_maker"`
	TradeTime		string	`json:"trade_time"`			// Transaction timestamp "2020-08-12T21:16:18.142746Z"
}

type Wallet struct {
	WalletBalance		float64	`json:"wallet_balance"`
	AvailableBalance	float64	`json:"available_balance"`
}
