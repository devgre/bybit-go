package ws

import "bitbucket.org/devgre/bybit-go/perpetual/api"

func (b *ByBitWS) processKLine(symbol string, data ...KLine) {
	b.Emit(TopicKLine, symbol, data)
}

func (b *ByBitWS) processOrder(data []api.Order) {
	b.Emit(TopicOrder, data)
}

func (b *ByBitWS) processPosition(data []api.Position) {
	b.Emit(TopicPosition, data)
}

func (b *ByBitWS) processExecution(data []Execution) {
	b.Emit(TopicExecution, data)
}

func (b *ByBitWS) processWallet(data []Wallet) {
	b.Emit(TopicWallet, data)
}
