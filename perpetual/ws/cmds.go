package ws

import "encoding/json"

// Cmd
// ws.send('{"op":"subscribe","args":["topic","topic.filter"]}');
// ws.send('{"op":"subscribe","args":["kline.BTCUSD.1m|3m"]}');
// ws.send('{"op":"subscribe","args":["kline.*.*"]}')
type Cmd struct {
	Op   string        `json:"op"`
	Args []interface{} `json:"args"`
}

func (c *Cmd) toBytes() []byte {
	bytes, _ := json.Marshal(c)
	return bytes
}

func (c *Cmd) toString() string {
	return string(c.toBytes())
}