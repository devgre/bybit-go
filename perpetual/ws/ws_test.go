package ws

import (
	"fmt"
	"testing"
	"time"

	bybit_go "bitbucket.org/devgre/bybit-go"
	"bitbucket.org/devgre/bybit-go/perpetual/api"
)

const (
	// Main
	//BaseUrl = "wss://stream.bybit.com"
	//ApiKey = "1wzxIKLfWjUIZFSVcA"
	//SecretKey = "DxXI8cqWozzGtshPkhPm8iqtVmhQfJDDjBYY"

	// Test
	BaseUrl   = "wss://stream-testnet.bybit.com"
	ApiKey    = "bLVooDimjXst3TpibW"
	SecretKey = "cCTjLc8AoLrg9wlJqxyqtrbOdEikY0uZc35X"
)

func newPerpetualWS(withAuth bool) *ByBitWS {
	cfg := &bybit_go.Configuration{
		Addr:      BaseUrl,
		DebugMode: true,
	}
	if withAuth {
		cfg.ApiKey = ApiKey
		cfg.SecretKey = SecretKey
	}
	return NewWS(cfg)
}

func TestByBitWS_Public(t *testing.T) {
	b := newPerpetualWS(false)
	err := b.Connect()
	if err != nil {
		t.Error(err)
		return
	}

	b.On(TopicKLine, func(symbol string, data []KLine) { fmt.Printf("handleKLine: %s/%v\n", symbol, data) })
	kLineTopicArg := TopicKLine + "." + Period5m + ".BTCUSDT"
	if err = b.Subscribe(kLineTopicArg, false); err != nil {
		t.Error(err)
	}

	forever := make(chan struct{})
	time.AfterFunc(time.Second*300, func() {
		forever <- struct{}{}
	})
	<-forever

	b.Disconnect()
	time.Sleep(time.Millisecond * 500)
	t.Log("Finished!")
}

func TestByBitWS_Private(t *testing.T) {
	b := newPerpetualWS(true)
	err := b.Connect()
	if err != nil {
		t.Error(err)
		return
	}

	time.Sleep(time.Millisecond * 500)

	b.On(TopicOrder, func(data []api.Order) { fmt.Printf("handleOrder: %v\n", data) })
	b.On(TopicPosition, func(data []api.Position) { fmt.Printf("handlePosition: %v\n", data) })
	b.On(TopicExecution, func(data []Execution) { fmt.Printf("handleExecution: %v\n", data) })
	b.On(TopicWallet, func(data []Wallet) { fmt.Printf("handleWallet: %v\n", data) })

	time.Sleep(time.Millisecond * 300)

	if err = b.Subscribe(TopicOrder, true); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicPosition, true); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicExecution, true); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicWallet, true); err != nil {
		t.Error(err)
	}

	forever := make(chan struct{})
	<-forever
}

func TestByBitWS(t *testing.T) {
	b := newPerpetualWS(true)
	err := b.Connect()
	if err != nil {
		t.Error(err)
		return
	}

	time.Sleep(time.Millisecond * 500)

	b.On(TopicKLine, func(symbol string, data []KLine) { fmt.Printf("handleKLine: %s/%v\n", symbol, data) })
	b.On(TopicOrder, func(data []api.Order) { fmt.Printf("handleOrder: %v\n", data) })
	b.On(TopicPosition, func(data []api.Position) { fmt.Printf("handlePosition: %v\n", data) })
	b.On(TopicExecution, func(data []Execution) { fmt.Printf("handleExecution: %v\n", data) })
	b.On(TopicWallet, func(data []Wallet) { fmt.Printf("handleWallet: %v\n", data) })

	kLineTopicArg := TopicKLine + "." + Period5m + ".AVAXUSDT"
	if err = b.Subscribe(kLineTopicArg, false); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicOrder, true); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicPosition, true); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicExecution, true); err != nil {
		t.Error(err)
	}
	if err = b.Subscribe(TopicWallet, true); err != nil {
		t.Error(err)
	}

	forever := make(chan struct{})
	<-forever
}
