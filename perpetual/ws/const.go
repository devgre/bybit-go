package ws

const (
	TopicKLine     = "candle" // candle.1.BTCUSDT
	TopicOrder     = "order"
	TopicPosition  = "position"
	TopicExecution = "execution"
	TopicWallet    = "wallet"

	Period1m  = "1"
	Period5m  = "5"
	Period15m = "15"
	Period30m = "30"
	Period1h  = "60"
	Period4h  = "240"
	Period12h = ""
	Period1d  = "D"
	Period1w  = "W"
	Period1M  = "M"
)
