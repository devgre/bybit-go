package api

import (
	"errors"
	"fmt"
	"net/http"

	bybit_go "bitbucket.org/devgre/bybit-go"
)

type StopOrder struct {
	OrderId        string  `json:"stop_order_id"`    //
	UserId         int     `json:"user_id"`          //
	Symbol         string  `json:"symbol"`           //
	Side           string  `json:"side"`             //
	OrderType      string  `json:"order_type"`       //
	Price          float64 `json:"price"`            //
	Qty            float64 `json:"qty"`              // Order quantity in USD
	TimeInForce    string  `json:"time_in_force"`    //
	OrderStatus    string  `json:"order_status"`     //
	TriggerPrice   float64 `json:"trigger_price"`    // If stop_order_type is TrailingProfit, this field is the trailing stop active price.
	OrderLinkId    string  `json:"order_link_id"`    // Customised order ID
	CreatedTime    string  `json:"created_time"`     //
	UpdatedTime    string  `json:"updated_time"`     //
	TakeProfit     float64 `json:"take_profit"`      //
	StopLoss       float64 `json:"stop_loss"`        //
	TpTriggerBy    string  `json:"tp_trigger_by"`    // Take profit trigger price type, default: LastPrice
	SlTriggerBy    string  `json:"sl_trigger_by"`    // Stop loss trigger price type, default: LastPrice
	BasePrice      string  `json:"base_price"`       // Market price at placing order
	TriggerBy      string  `json:"trigger_by"`       // Trigger price type. Default LastPrice
	ReduceOnly     bool    `json:"reduce_only"`      // true means close order, false means open position
	CloseOnTrigger bool    `json:"close_on_trigger"` // Is close on trigger order
	PositionIdx    int     `json:"position_idx"`     // 0-One-Way Mode, 1-Buy side of both side mode, 2-Sell side of both side mode
}

type PlaceStopOrderResult struct {
	bybit_go.BaseResult
	Result StopOrder `json:"result"`
}

type QueryStopOrderResult struct {
	bybit_go.BaseResult
	Result StopOrder `json:"result"`
}

type QueryStopOrdersResult struct {
	bybit_go.BaseResult
	Result []StopOrder `json:"result"`
}

type CancelStopOrderResult struct {
	bybit_go.BaseResult
	Result struct {
		OrderId string `json:"stop_order_id"`
	} `json:"result"`
}

type CancelAllStopOrdersResult struct {
	bybit_go.BaseResult
	Result []string `json:"result"`
}

// PlaceStopOrder
// https://bybit-exchange.github.io/docs/linear/?console#t-placecond
func (b *ByBit) PlaceStopOrder(symbol string, orderType string, orderSide string, timeInForce string,
	price, qty float64, basePrice float64, stopPx float64,
	triggerBy string, reduceOnly bool) (result StopOrder, err error) {
	var ret PlaceStopOrderResult
	params := map[string]interface{}{}
	params["symbol"] = symbol
	params["order_type"] = orderType
	params["side"] = orderSide
	params["time_in_force"] = timeInForce
	params["qty"] = qty
	if price > 0 {
		params["price"] = price
	}
	params["base_price"] = basePrice
	params["stop_px"] = stopPx
	if reduceOnly {
		params["reduce_only"] = true
	}
	if triggerBy != "" {
		params["trigger_by"] = triggerBy
	}

	err = b.SignedRequest(http.MethodPost, "/private/linear/stop-order/create", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = fmt.Errorf("[%d] - %s", ret.RetCode, ret.RetMsg)
		return
	}
	result = ret.Result
	return
}

// CancelStopOrder
// https://bybit-exchange.github.io/docs/linear/?console#t-cancelcond
func (b *ByBit) CancelStopOrder(symbol, orderId, orderLinkId string) (canceledOrderId string, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}
	if orderId != "" {
		params["orderId"] = orderId
	}
	if orderLinkId != "" {
		params["order_link_id"] = orderLinkId
	}

	var ret CancelStopOrderResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/stop-order/cancel", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	canceledOrderId = ret.Result.OrderId
	return
}

// CancelAllStopOrder
// https://bybit-exchange.github.io/docs/linear/?console#t-cancelallcond
func (b *ByBit) CancelAllStopOrder(symbol string) (canceledOrderIds []string, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}

	var ret CancelAllStopOrdersResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/stop-order/cancel-all", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	canceledOrderIds = ret.Result
	return
}

// QueryOpenedStopOrders
// https://bybit-exchange.github.io/docs/linear/?console#t-querycond
func (b *ByBit) QueryOpenedStopOrders(symbol, orderId, orderLinkId string) (orders []StopOrder, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}
	if orderId != "" {
		params["orderId"] = orderId
	}
	if orderLinkId != "" {
		params["order_link_id"] = orderLinkId
	}

	var ret interface{}
	err = b.SignedRequest(http.MethodGet, "/private/linear/order/search", params, &ret)
	if err != nil {
		return
	}

	retVal, ok := ret.(QueryStopOrderResult)
	if ok {
		orders = []StopOrder{retVal.Result}
		return
	} else {
		retVal, ok := ret.(QueryStopOrdersResult)
		if ok {
			orders = retVal.Result
			return
		}
	}
	return
}
