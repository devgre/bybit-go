package api

import (
	"testing"
)

func TestByBit_PlaceOrder(t *testing.T) {
	var positionIDx int
	b := newByBit()
	qty := 1.0
	price := 10.0
	takeProfit := 0.0
	stopLoss := 0.0
	tpTriggerBy := "MarkPrice" // LastPrice IndexPrice MarkPrice
	slTriggerBy := "MarkPrice" // LastPrice IndexPrice MarkPrice
	positionIDx = 0
	res, err := b.PlaceOrder(
		"APTUSDT",
		"Limit",
		"Buy",
		"GoodTillCancel",
		price, qty, takeProfit, stopLoss, tpTriggerBy, slTriggerBy, false, false, &positionIDx, "")
	if err != nil {
		t.Error("PlaceOrder failed: ", err)
		return
	}

	t.Log(res)
	t.Logf("OrderId: %s. Use it for order cancellation", res.OrderId)
}

func TestByBit_CancelOrder(t *testing.T) {
	b := newByBit()

	orderId, err := b.CancelOrder("APTUSDT", "ecea536d-1a91-466a-bccd-ddbe66326a6e", "b333458e-e26a-4a4c-9e3d-af91ad289f6b")
	if err != nil {
		t.Errorf("CancelOrder failed: %v", err)
		return
	}

	t.Logf("Order cancelled: %s", orderId)
}

func TestByBit_CancelAllOrder(t *testing.T) {
	b := newByBit()

	orderIds, err := b.CancelAllOrder("APTUSDT")
	if err != nil {
		t.Errorf("CancelAllOrder failed: %v", err)
		return
	}

	t.Logf("Orders cancelled: %v", orderIds)
}

func TestByBit_QueryOpenedOrders(t *testing.T) {
	b := newByBit()

	res, err := b.QueryOpenedOrders("AXSUSDT", "", "")
	if err != nil {
		t.Error("QueryOpenedOrders failed: ", err)
		return
	}

	t.Logf("Orders: %v", res)
}
