package api

import (
	"errors"
	"fmt"
	"net/http"

	bybit_go "bitbucket.org/devgre/bybit-go"
)

type Position struct {
	Symbol              string  `json:"symbol"`         //
	Side                string  `json:"side"`           //
	Size                float64 `json:"size"`           //
	PositionValue       float64 `json:"position_value"` // Current position value
	EntryPrice          float64 `json:"entry_price"`    // Average opening price
	LiqPrice            float64 `json:"liq_price"`      // Liquidation price
	BustPrice           float64 `json:"bust_price"`
	Leverage            float64 `json:"leverage"`        // In Isolated Margin mode, the value is set by user. In Cross Margin mode, the value is the max leverage at current risk level
	AutoAddMargin       string  `json:"auto_add_margin"` // Whether to add margin automatically
	PositionMargin      float64 `json:"position_margin"`
	OccClosingFee       float64 `json:"occ_closing_fee"` // Pre-occupancy closing fee
	RealisedPnl         float64 `json:"realised_pnl"`
	CumRealisedPnl      float64 `json:"cum_realised_pnl"` // Cumulative realised Profit and Loss
	FreeQty             float64 `json:"free_qty"`         // Qty which can be closed
	TpSlMode            string  `json:"tp_sl_mode"`       // Stop loss and take profit mode Full or Partial
	UnrealisedPnl       float64 `json:"unrealised_pnl"`
	DeleverageIndicator float64 `json:"deleverage_indicator"` // Deleverage indicator level (1,2,3,4,5)
	TakeProfit          float64 `json:"take_profit"`
	TpTriggerBy         string  `json:"tp_trigger_by"` // Take profit trigger price type, default: LastPrice
	StopLoss            float64 `json:"stop_loss"`
	SlTriggerBy         string  `json:"sl_trigger_by"`
	TrailingStop        float64 `json:"trailing_stop"`   // 0-One-Way Mode, 1-Buy side of both side mode, 2-Sell side of both side mode
	PositionStatus      string  `json:"position_status"` // Position status: Normal, Liq, Adl
	PositionSeq         string  `json:"position_seq"`    // Position sequence
	//PositionIdx			string	`json:"position_idx"`			// 0-One-Way Mode, 1-Buy side of both side mode, 2-Sell side of both side mode
	Isolated   bool `json:"isolated,omitempty"`
	IsIsolated bool `json:"is_isolated,omitempty"` // true means isolated margin mode; false means cross margin mode
}

type GetPositionsResult struct {
	bybit_go.BaseResult
	Result []Position `json:"result"`
}

// GetPositions
// https://bybit-exchange.github.io/docs/linear/?console#t-myposition
func (b *ByBit) GetPositions(symbol string) (positions []Position, err error) {
	params := map[string]interface{}{}
	if symbol != "" {
		params["symbol"] = symbol
	}

	var ret GetPositionsResult
	err = b.SignedRequest(http.MethodGet, "/private/linear/position/list", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	positions = ret.Result
	return
}

// MarginModeSwitch
// https://bybit-exchange.github.io/docs/linear/?console#t-marginswitch
func (b *ByBit) MarginModeSwitch(symbol string, isIsolated bool, buyLeverage, sellLeverage float64) (err error) {
	params := map[string]interface{}{
		"symbol":        symbol,
		"is_isolated":   isIsolated,
		"buy_leverage":  buyLeverage,
		"sell_leverage": sellLeverage,
	}

	var ret bybit_go.BaseResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/position/switch-isolated", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}
	return
}

// PositionModeSwitch
// https://bybit-exchange.github.io/docs/linear/?console#t-switchpositionmode
func (b *ByBit) PositionModeSwitch(symbol, mode string) (err error) {
	params := map[string]interface{}{
		"symbol": symbol,
		"mode":   mode,
	}

	var ret bybit_go.BaseResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/position/switch-mode", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}
	return
}

// SetLeverage
// https://bybit-exchange.github.io/docs/linear/?console#t-setleverage
func (b *ByBit) SetLeverage(symbol string, buyLeverage, sellLeverage float64) (err error) {
	params := map[string]interface{}{
		"symbol":        symbol,
		"buy_leverage":  buyLeverage,
		"sell_leverage": sellLeverage,
	}

	var ret bybit_go.BaseResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/position/set-leverage", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}
	return
}

func (b *ByBit) SetTradingStop(symbol, side string,
	takeProfit, stopLoss, trailingStop float64,
	tpTriggerBy, slTriggerBy string,
	tpSize, slSize float64, positionIDx *int) (err error) {
	params := map[string]interface{}{
		"symbol": symbol, "side": side,
	}
	if takeProfit > 0 {
		params["take_profit"] = takeProfit
	}
	if stopLoss > 0 {
		params["stop_loss"] = stopLoss
	}
	if trailingStop > 0 {
		params["trailing_stop"] = trailingStop
	}
	if tpTriggerBy != "" {
		params["tp_trigger_by"] = tpTriggerBy
	}
	if slTriggerBy != "" {
		params["sl_trigger_by"] = slTriggerBy
	}
	if tpSize > 0 {
		params["tp_size"] = tpSize
	}
	if slSize > 0 {
		params["sl_size"] = slSize
	}
	if positionIDx != nil {
		params["position_idx"] = *positionIDx
	}

	var ret bybit_go.BaseResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/position/trading-stop", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}
	return
}
