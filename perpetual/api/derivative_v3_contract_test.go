package api

import (
	"testing"
)

func TestByBit_DerivativeV3ContractCancelOrder(t *testing.T) {
	b := newByBit()

	orderId, err := b.DerivativeV3ContractCancelOrder("AXSUSDT", "6d76df37-6ada-4a9c-be0b-cce33c4cda18")
	if err != nil {
		t.Errorf("DerivativeV3ContractCancelOrder failed: %v", err)
		return
	}

	t.Logf("Order cancelled: %s", orderId)
}
