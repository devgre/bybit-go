package api

import (
	"testing"
)

func TestByBit_GetPositions(t *testing.T) {
	b := newByBit()

	pos, err := b.GetPositions("AXSUSDT")
	if err != nil {
		t.Errorf("GetPositions failed: %v", err)
		return
	}
	t.Logf("Positions: %v", pos)
}

func TestByBit_MarginModeSwitch(t *testing.T) {
	b := newByBit()

	err := b.MarginModeSwitch("AXSUSDT", true, 1.0, 1.0)
	if err != nil {
		t.Errorf("MarginModeSwitch failed: %v", err)
	}
}

func TestByBit_PositionModeSwitch(t *testing.T) {
	b := newByBit()

	err := b.PositionModeSwitch("AXSUSDT", "MergedSingle") // MergedSingle or BothSide
	if err != nil {
		t.Errorf("PositionModeSwitch failed: %v", err)
	}
}

func TestByBit_SetLeverage(t *testing.T) {
	b := newByBit()

	err := b.SetLeverage("AXSUSDT", 10.0, 1.0)
	if err != nil {
		t.Errorf("SetLeverage failed: %v", err)
	}
}

func TestByBit_SetTradingStop(t *testing.T) {
	var positionIDx int
	b := newByBit()

	//price := 65.45
	takeProfit := 66.0
	stopLoss := 61.15
	tpTriggerBy := "MarkPrice" // LastPrice IndexPrice MarkPrice
	slTriggerBy := "MarkPrice" // LastPrice IndexPrice MarkPrice
	positionIDx = 0
	err := b.SetTradingStop("AXSUSDT", "Buy",
		takeProfit, stopLoss, 0, tpTriggerBy, slTriggerBy, 0, 0.0, &positionIDx)
	if err != nil {
		t.Errorf("SetTradingStop failed: %v", err)
	}
}

func TestByBit_GetWalletBalance(t *testing.T) {
	b := newByBit()

	res, err := b.GetWalletBalance()
	if err != nil {
		t.Error("GetWalletBalance failed: ", err)
		return
	}

	t.Logf("Balance: %v", res)
}
