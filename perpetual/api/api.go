package api

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	bybit_go "bitbucket.org/devgre/bybit-go"
)

type ByBit struct {
	bybit_go.BaseApi
}

func NewApi(httpClient *http.Client, baseURL string, apiKey string, secretKey string, debugMode bool) *ByBit {
	if httpClient == nil {
		httpClient = &http.Client{
			Timeout: 10 * time.Second,
		}
	}

	return &ByBit{bybit_go.BaseApi{
		BaseURL:   baseURL,
		ApiKey:    apiKey,
		SecretKey: secretKey,
		Client:    httpClient,
		DebugMode: debugMode,
	}}
}

func (b *ByBit) SetCorrectServerTime() (err error) {
	var timeNow int64
	timeNow, err = b.GetServerTime()
	if err != nil {
		return
	}
	b.ServerTimeOffset = timeNow - time.Now().UnixNano()/1e6
	return
}

// GetServerTime
// https://bybit-exchange.github.io/docs/linear/?console#t-servertime
// timeNow - ms
func (b *ByBit) GetServerTime() (timeNow int64, err error) {
	params := map[string]interface{}{}
	var ret bybit_go.BaseResult
	if err = b.Request(http.MethodGet, "/v2/public/time", params, &ret); err != nil {
		return
	}
	var t float64
	t, err = strconv.ParseFloat(ret.TimeNow, 64)
	if err != nil {
		return
	}
	timeNow = int64(t * 1000)
	return
}

// GetKLine (USDT Perpetual)
// https://bybit-exchange.github.io/docs/linear/?console#t-querykline
// interval: 1 3 5 15 30 60 120 240 360 720 "D" "M" "W" "Y"
// from: From timestamp in seconds
// limit: Limit for data size per page, max size is 200. Default as showing 200 pieces of data per page
func (b *ByBit) GetKLine(symbol string, interval string, from int64, limit int) (result []KLine, err error) {
	var ret GetKlineResult
	params := map[string]interface{}{}
	params["symbol"] = symbol
	params["interval"] = interval
	params["from"] = from
	if limit > 0 {
		params["limit"] = limit
	}
	err = b.Request(http.MethodGet, "/public/linear/kline", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}
	result = ret.Result
	return
}

func (b *ByBit) GetWalletBalance() (balance map[string]Balance, err error) {
	params := map[string]interface{}{}

	var ret BalancesResult
	err = b.SignedRequest(http.MethodGet, "/v2/private/wallet/balance", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	balance = ret.Result
	return
}

func (b *ByBit) QuerySymbol() (symbolInfo []SymbolInfo, err error) {
	params := map[string]interface{}{}

	var ret SymbolInfoResult
	err = b.SignedRequest(http.MethodGet, "/v2/public/symbols", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	symbolInfo = ret.Result
	return
}
