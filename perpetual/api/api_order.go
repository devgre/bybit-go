package api

import (
	"errors"
	"fmt"
	"net/http"

	bybit_go "bitbucket.org/devgre/bybit-go"
)

type Order struct {
	OrderId        string  `json:"order_id"`         //
	OrderLinkId    string  `json:"order_link_id"`    // Customised order ID
	UserId         int     `json:"user_id"`          //
	Symbol         string  `json:"symbol"`           //
	Side           string  `json:"side"`             //
	OrderType      string  `json:"order_type"`       //
	Price          float64 `json:"price"`            //
	Qty            float64 `json:"qty"`              // Transaction qty
	LeavesQty      float64 `json:"leaves_qty"`       // Number of unfilled contracts from the order's size
	LastExecPrice  float64 `json:"last_exec_price"`  // Last execution price
	CumExecQty     float64 `json:"cum_exec_qty"`     // Cumulative qty of trading
	CumExecValue   float64 `json:"cum_exec_value"`   // Cumulative value of trading
	CumExecFee     float64 `json:"cum_exec_fee"`     // Cumulative trading fees
	TimeInForce    string  `json:"time_in_force"`    //
	OrderStatus    string  `json:"order_status"`     //
	ReduceOnly     bool    `json:"reduce_only"`      // true means close order, false means open position
	CloseOnTrigger bool    `json:"close_on_trigger"` // Is close on trigger order
	CreatedTime    string  `json:"created_time"`     //
	UpdatedTime    string  `json:"updated_time"`     //
	TakeProfit     float64 `json:"take_profit"`      //
	StopLoss       float64 `json:"stop_loss"`        //
	TrailingStop   float64 `json:"trailing_stop"`    //
	TpTriggerBy    string  `json:"tp_trigger_by"`    // Take profit trigger price type, default: LastPrice
	SlTriggerBy    string  `json:"sl_trigger_by"`    // Stop loss trigger price type, default: LastPrice

	// Position idx, used to identify positions in different position modes. Required if you are under One-Way Mode:
	// 0-One-Way Mode, 1-Buy side of both side mode, 2-Sell side of both side mode
	//PositionIdx string `json:"position_idx"`
}

type PlaceOrderResult struct {
	bybit_go.BaseResult
	Result Order `json:"result"`
}

type QueryOrdersResult struct {
	bybit_go.BaseResult
	Result []Order `json:"result"`
}

type CancelOrderResult struct {
	bybit_go.BaseResult
	Result struct {
		OrderId string `json:"order_id"`
	} `json:"result"`
}

type CancelAllOrdersResult struct {
	bybit_go.BaseResult
	Result []string `json:"result"`
}

// PlaceOrder
// https://bybit-exchange.github.io/docs/linear/?console#t-placeactive
func (b *ByBit) PlaceOrder(symbol string, orderType string, orderSide string, timeInForce string,
	price, qty, takeProfit, stopLoss float64, tpTriggerBy, slTriggerBy string,
	closeOnTrigger, reduceOnly bool, positionIDx *int, orderLinkId string) (result Order, err error) {
	var ret PlaceOrderResult
	params := map[string]interface{}{}
	params["symbol"] = symbol
	params["order_type"] = orderType
	params["side"] = orderSide
	params["time_in_force"] = timeInForce
	params["qty"] = qty
	if price > 0 {
		params["price"] = price
	}
	if takeProfit > 0 {
		params["take_profit"] = takeProfit
	}
	if stopLoss > 0 {
		params["stop_loss"] = stopLoss
	}
	if tpTriggerBy != "" {
		params["tp_trigger_by"] = tpTriggerBy
	}
	if slTriggerBy != "" {
		params["sl_trigger_by"] = slTriggerBy
	}
	params["close_on_trigger"] = closeOnTrigger
	params["reduce_only"] = reduceOnly
	if orderLinkId != "" {
		params["order_link_id"] = orderLinkId
	}
	if positionIDx != nil {
		params["position_idx"] = *positionIDx
	}

	err = b.SignedRequest(http.MethodPost, "/private/linear/order/create", params, &ret)
	if err != nil {
		return
	}
	if ret.RetCode != 0 {
		err = fmt.Errorf("[%d] - %s", ret.RetCode, ret.RetMsg)
		return
	}
	result = ret.Result
	return
}

// CancelOrder
// https://bybit-exchange.github.io/docs/linear/?console#t-cancelactive
func (b *ByBit) CancelOrder(symbol, orderId, orderLinkId string) (canceledOrderId string, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}
	if orderId != "" {
		params["orderId"] = orderId
	}
	if orderLinkId != "" {
		params["order_link_id"] = orderLinkId
	}

	var ret CancelOrderResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/order/cancel", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	canceledOrderId = ret.Result.OrderId
	return
}

// CancelAllOrder
// https://bybit-exchange.github.io/docs/linear/?console#t-cancelallactive
func (b *ByBit) CancelAllOrder(symbol string) (canceledOrderIds []string, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}

	var ret CancelAllOrdersResult
	err = b.SignedRequest(http.MethodPost, "/private/linear/order/cancel-all", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	canceledOrderIds = ret.Result
	return
}

// QueryOpenedOrders
// https://bybit-exchange.github.io/docs/linear/?console#t-queryactive
func (b *ByBit) QueryOpenedOrders(symbol, orderId, orderLinkId string) (orders []Order, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}
	if orderId != "" {
		params["orderId"] = orderId
	}
	if orderLinkId != "" {
		params["order_link_id"] = orderLinkId
	}

	var ret QueryOrdersResult
	err = b.SignedRequest(http.MethodGet, "/private/linear/order/search", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	orders = ret.Result
	return
}
