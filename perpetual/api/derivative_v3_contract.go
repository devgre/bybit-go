package api

import (
	"errors"
	"fmt"
	"net/http"
)

// DerivativeV3ContractCancelOrder
// https://bybit-exchange.github.io/docs/derivativesV3/contract/#t-contract_cancelorder
func (b *ByBit) DerivativeV3ContractCancelOrder(symbol, orderId string) (canceledOrderId string, err error) {
	params := map[string]interface{}{
		"symbol":  symbol,
		"orderId": orderId,
	}

	var ret CancelOrderResult
	err = b.SignedRequest(http.MethodPost, "/contract/v3/order/cancel", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	canceledOrderId = ret.Result.OrderId
	return
}
