package api

import bybit_go "bitbucket.org/devgre/bybit-go"

type KLine struct {
	Symbol   string  `json:"symbol"`
	Interval string  `json:"interval"`
	OpenTime int64   `json:"open_time"`
	Open     float64 `json:"open"`
	High     float64 `json:"high"`
	Low      float64 `json:"low"`
	Close    float64 `json:"close"`
	Volume   float64 `json:"volume"`
	Turnover float64 `json:"turnover"`
}

type GetKlineResult struct {
	bybit_go.BaseResult
	Result []KLine `json:"result"`
}

type Balance struct {
	Equity           float64 `json:"equity"`
	AvailableBalance float64 `json:"available_balance"` // Available balance = wallet balance - used margin
	UsedMargin       float64 `json:"used_margin"`       // Used margin
	OrderMargin      float64 `json:"order_margin"`      // Pre-occupied order margin
	PositionMargin   float64 `json:"position_margin"`   // Position margin
	OccClosingFee    float64 `json:"occ_closing_fee"`   // Position closing fee occupied (your opening fee + expected maximum closing fee)
	OccFundingFee    float64 `json:"occ_funding_fee"`   // Pre-occupied funding fee: calculated from position qty and current funding fee
	WalletBalance    float64 `json:"wallet_balance"`    //
	RealisedPnl      float64 `json:"realised_pnl"`      // Today's realised pnl
	UnrealisedPnl    float64 `json:"unrealised_pnl"`    //         unrealised pnl
	CumRealisedPnl   float64 `json:"cum_realised_pnl"`  // Accumulated realised pnl (all-time total)
	GivenCash        float64 `json:"given_cash"`
	ServiceCash      float64 `json:"service_cash"` // Service cash is used for user's service charge
}

type BalancesResult struct {
	bybit_go.BaseResult
	Result map[string]Balance `json:"result"`
}

type SymbolInfo struct {
	Name            string `json:"name"`
	Alias           string `json:"alias"`
	Status          string `json:"status"`
	BaseCurrency    string `json:"base_currency"`
	QuoteCurrency   string `json:"quote_currency"`
	PriceScale      int    `json:"price_scale"`
	TakerFee        string `json:"taker_fee"`
	MakerFee        string `json:"maker_fee"`
	FundingInterval int    `json:"funding_interval"`
	LeverageFilter  struct {
		MinLeverage  int    `json:"min_leverage"`
		MaxLeverage  int    `json:"max_leverage"`
		LeverageStep string `json:"leverage_step"`
	} `json:"leverage_filter"`
	PriceFilter struct {
		MinPrice string `json:"min_price"`
		MaxPrice string `json:"max_price"`
		TickSize string `json:"tick_size"`
	} `json:"price_filter"`
	LotSizeFilter struct {
		MaxTradingQty float64 `json:"max_trading_qty"`
		MinTradingQty float64 `json:"min_trading_qty"`
		QtyStep       float64 `json:"qty_step"`
	} `json:"lot_size_filter"`
}

type SymbolInfoResult struct {
	bybit_go.BaseResult
	Result []SymbolInfo `json:"result"`
}
