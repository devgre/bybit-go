package api

import (
	"log"
	"testing"
	"time"

	bybit_go "bitbucket.org/devgre/bybit-go"
	"bitbucket.org/devgre/bybit-go/utils"
)

const (
	BaseUrl   = bybit_go.TestnetEndpoint
	ApiKey    = "Jx30r7IXObUDSsIBTF"
	SecretKey = "5uJd3rJ50p4LhR6VRbX5zFuMw3osFZfG1N4B"
)

func newByBit() *ByBit {
	baseURL := BaseUrl
	apiKey := ApiKey
	secretKey := SecretKey
	b := NewApi(nil, baseURL, apiKey, secretKey, true)
	err := b.SetCorrectServerTime()
	if err != nil {
		log.Printf("%v", err)
	}
	return b
}

func TestByBit_GetServerTime(t *testing.T) {
	b := newByBit()
	ms, err := b.GetServerTime()
	if err != nil {
		t.Error("GetServerTime failed: ", err)
	}
	t.Log(ms, utils.TimeFromUnixTime(ms))
}

func TestByBit_GetKLine(t *testing.T) {
	b := newByBit()

	//from := time.Now().Add(-1 * time.Hour).Unix()
	from := time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC).Unix()
	res, err := b.GetKLine("AVAXUSDT", "5", from, 10)
	if err != nil {
		t.Error("GetKLine failed: ", err)
	}
	for _, v := range res {
		t.Logf("%#v", v)
	}
}

func TestGetWalletBalance(t *testing.T) {
	b := newByBit()

	res, err := b.GetWalletBalance()
	if err != nil {
		t.Error("GetWalletBalance failed: ", err)
	}
	t.Logf("Balance: %v", res)
}

func TestByBit_QuerySymbol(t *testing.T) {
	b := newByBit()

	res, err := b.QuerySymbol()
	if err != nil {
		t.Error("QuerySymbol failed: ", err)
	}
	for _, v := range res {
		t.Logf("%#v", v)
	}
}
