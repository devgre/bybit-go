package ws

import (
	"encoding/json"
	"errors"
	"log"

	bybit_go "bitbucket.org/devgre/bybit-go"
	"github.com/tidwall/gjson"
)

const (
	PublicEndpointSpot  = "/spot/quote/ws/v2"
	PrivateEndpointSpot = "/spot/ws"

	TopicKLine               = "kline"
	TopicOutboundAccountInfo = "outboundAccountInfo"
	TopicExecutionReport     = "executionReport"

	Period1m  = "1m"
	Period5m  = "5m"
	Period15m = "15m"
	Period30m = "30m"
	Period1h  = "1h"
	Period4h  = "4h"
	Period12h = "12h"
	Period1d  = "1d"
	Period1w  = "1w"
	Period1M  = "1M"
)

type ByBitSpotWS struct {
	*bybit_go.BaseWS
	private *bybit_go.BaseWS

	DebugMode bool
}

func NewSpotWS(config *bybit_go.Configuration) *ByBitSpotWS {
	cfg := &bybit_go.Configuration{
		Addr:      config.Addr + PublicEndpointSpot,
		DebugMode: config.DebugMode,
	}

	b := &ByBitSpotWS{
		DebugMode: cfg.DebugMode,
	}
	b.BaseWS = bybit_go.NewBaseWS(cfg)

	if config.ApiKey != "" && config.SecretKey != "" {
		cfg2 := &bybit_go.Configuration{
			Addr:      config.Addr + PrivateEndpointSpot,
			ApiKey:    config.ApiKey,
			SecretKey: config.SecretKey,
			DebugMode: config.DebugMode,
		}
		b.private = bybit_go.NewBaseWS(cfg2)
	}
	return b
}

func (b *ByBitSpotWS) Connect() error {
	var err error
	if err = b.Start(nil, b.processMessage); err != nil {
		return err
	}

	if b.private != nil {
		if err = b.private.Start(nil, b.processMessage); err != nil {
			return err
		}
	}

	return nil
}

func (b *ByBitSpotWS) Subscribe(cmd CmdKLineSpot, privateTopic bool) error {
	//if privateTopic {
	//	b.private.subscribeCmds = append(b.subscribeCmds, cmd.toString())
	//	return b.private.SendBCmd(cmd.toBytes())
	//}
	//
	//b.subscribeCmds = append(b.subscribeCmds, cmd.toString())
	//return b.SendBCmd(cmd.toBytes())
	return errors.New("not implemented")
}

func (b *ByBitSpotWS) Unsubscribe(cmd CmdKLineSpot, privateTopic bool) error {
	if privateTopic {
		return b.private.SendBCmd(cmd.toBytes())
	}
	return b.SendBCmd(cmd.toBytes())
}

func (b *ByBitSpotWS) processMessage(messageType int, data []byte) {
	ret := gjson.ParseBytes(data)

	if pong := ret.Get("pong"); pong.Exists() {
		//log.Println(utils.TimeFromUnixTime(pong.Int()))  // todo: correct server time here ?
		//b.handlePong()
		return
	}

	if b.DebugMode {
		log.Printf("%v", string(data))
	}

	if topicValue := ret.Get("topic"); topicValue.Exists() {
		topic := topicValue.String()
		if topic == TopicKLine {
			if retData := ret.Get("data"); retData.Exists() {
				symbol := ret.Get("params").Get("symbol").String()

				var data KLineSpot
				err := json.Unmarshal([]byte(retData.Raw), &data)
				if err != nil {
					log.Printf("%v", err)
					return
				}
				b.processKLine(symbol, data)
			}
		}
	} else if eventTypeValue := ret.Get("0.e"); eventTypeValue.Exists() {
		eventType := eventTypeValue.String()
		if eventType == TopicOutboundAccountInfo {
			var info []OutboundAccountInfoResult
			err := json.Unmarshal([]byte(ret.Raw), &info)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processOutboundAccountInfo(info)
		} else if eventType == TopicExecutionReport {
			var execReport []ExecutionReport
			err := json.Unmarshal([]byte(ret.Raw), &execReport)
			if err != nil {
				log.Printf("%v", err)
				return
			}
			b.processExecutionReport(execReport)
		}
	}

}
