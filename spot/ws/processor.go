package ws

func (b *ByBitSpotWS) processKLine(symbol string, data KLineSpot) {
	b.Emit(TopicKLine, symbol, data)
}

func (b *ByBitSpotWS) processOutboundAccountInfo(info []OutboundAccountInfoResult) {
	b.Emit(TopicOutboundAccountInfo, info)
}

func (b *ByBitSpotWS) processExecutionReport(report []ExecutionReport) {
	b.Emit(TopicExecutionReport, report)
}
