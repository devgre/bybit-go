package ws

import (
	"fmt"
	"testing"
	"time"

	bybit_go "bitbucket.org/devgre/bybit-go"
)

const (
	// Main
	//BaseUrl = "wss://stream.bybit.com"
	//ApiKey = "1wzxIKLfWjUIZFSVcA"
	//SecretKey = "DxXI8cqWozzGtshPkhPm8iqtVmhQfJDDjBYY"

	// Test
	BaseUrl   = "wss://stream-testnet.bybit.com"
	ApiKey    = "bLVooDimjXst3TpibW"
	SecretKey = "cCTjLc8AoLrg9wlJqxyqtrbOdEikY0uZc35X"
)

func newSpotWS(withAuth bool) *ByBitSpotWS {
	cfg := &bybit_go.Configuration{
		Addr:      BaseUrl,
		DebugMode: true,
	}
	if withAuth {
		cfg.ApiKey = ApiKey
		cfg.SecretKey = SecretKey
	}
	return NewSpotWS(cfg)
}

func TestByBitSpotWS_Public(t *testing.T) {
	b := newSpotWS(false)
	err := b.Connect()
	if err != nil {
		t.Log(err)
		return
	}

	time.Sleep(time.Millisecond * 500)

	kLineCmd := CmdKLineSpot{Topic: TopicKLine, Symbol: "BTCUSDT", Event: "sub", Period: Period5m}
	b.Subscribe(kLineCmd, false)
	b.On(TopicKLine, handleSpotKLine)

	forever := make(chan struct{})
	<-forever
}

func TestByBitSpotWS_Private(t *testing.T) {
	b := newSpotWS(true)
	err := b.Connect()
	if err != nil {
		t.Log(err)
		return
	}

	b.On(TopicOutboundAccountInfo, handleOutboundInfo)
	b.On(TopicExecutionReport, handleExecutionReport)

	forever := make(chan struct{})
	<-forever
}

func TestByBitSpotWS_PublicPrivate(t *testing.T) {
	b := newSpotWS(true)
	err := b.Connect()
	if err != nil {
		t.Log(err)
	}

	time.Sleep(time.Millisecond * 500)

	kLineCmd := CmdKLineSpot{Topic: TopicKLine, Event: "sub", Symbol: "BTCUSDT", Period: Period5m}
	b.On(TopicKLine, handleSpotKLine)
	b.Subscribe(kLineCmd, false)

	b.On(TopicOutboundAccountInfo, handleOutboundInfo)
	b.On(TopicExecutionReport, handleExecutionReport)

	forever := make(chan struct{})
	<-forever
}

func handleSpotKLine(symbol string, data KLineSpot) {
	fmt.Printf("handleSpotKLine: %s/%v\n", symbol, data)
}

func handleOutboundInfo(info []OutboundAccountInfoResult) {
	fmt.Printf("handleOutboundInfo: %v\n", info)
}

func handleExecutionReport(info []ExecutionReport) {
	fmt.Printf("handleExecutionReport: %v\n", info)
}
