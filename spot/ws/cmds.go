package ws

import "encoding/json"

// CmdSpot
// ws.send('{"topic": "kline", "event": "sub", "params": { "symbol": "BTCUSDT", "klineType": "1m", "binary": false }}')
type CmdSpot struct {
	Symbol string                 `json:"symbol"`
	Topic  string                 `json:"topic"`
	Event  string                 `json:"event"`
	Params map[string]interface{} `json:"params"`
}

type CmdKLineSpot struct {
	Topic  string
	Symbol string
	Event  string
	Period string
}

func (c *CmdKLineSpot) toCmd() CmdSpot {
	return CmdSpot{
		Symbol: c.Symbol,
		Topic:  c.Topic,
		Event:  c.Event,
		Params: map[string]interface{}{
			"symbol":    c.Symbol,
			"klineType": c.Period,
			"binary":    false,
		},
	}
}
func (c *CmdKLineSpot) toBytes() []byte {
	bytes, _ := json.Marshal(c.toCmd())
	return bytes
}

func (c *CmdKLineSpot) toString() string {
	return string(c.toBytes())
}
