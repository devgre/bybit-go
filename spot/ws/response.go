package ws

type SubscriptionSpot struct {
	Code   string                 `json:"code"`
	Msg    string                 `json:"msg"`
	Topic  string                 `json:"topic"`
	Event  string                 `json:"event"`
	Symbol string                 `json:"symbol"`
	Params map[string]interface{} `json:"params"`
}

type KLineSpot struct {
	Symbol   string `json:"s"` // BTCUSD
	OpenTime int64  `json:"t"` // 1539918000
	Open     string `json:"o"`
	High     string `json:"h"`
	Low      string `json:"l"`
	Close    string `json:"c"`
	Volume   string `json:"v"`
}

type WalletBalance struct {
	Symbol string `json:"a"`
	Free   string `json:"f"`
	Locked string `json:"l"`
}

type OutboundAccountInfoResult struct {
	EventType      string          `json:"e"`
	Timestamp      string          `json:"E"`
	AllowTrade     bool            `json:"T"`
	AllowWithdraw  bool            `json:"W"`
	AllowDeposit   bool            `json:"D"`
	WalletBalances []WalletBalance `json:"B"`
}

type ExecutionReport struct {
	EventType       string `json:"e"`
	Timestamp       string `json:"E"`
	Symbol          string `json:"s"`
	UsersOrderId    string `json:"c"`
	OrderSide       string `json:"S"`
	OrderType       string `json:"o"`
	TimeInForce     string `json:"f"`
	Qty             string `json:"q"`
	Price           string `json:"p"`
	OrderStatus     string `json:"X"`
	OrderId         string `json:"i"`
	OppTradeOrderId string `json:"M"`
	LastFilledQty   string `json:"l"`
	TotalFilledQty  string `json:"z"`
	LastTradePrice  string `json:"L"`
	TradingFee      string `json:"n"`
	FeeAssetType    string `json:"N"`
	IsNormal        bool   `json:"u"`
	IsWorking       bool   `json:"w"`
	IsLimitMaker    bool   `json:"m"`
	OrderTime       string `json:"O"`
	TotalFilled     string `json:"Z"`
	OppAccountId    string `json:"A"`
	IsClose         bool   `json:"C"`
	Leverage        string `json:"v"`
}
