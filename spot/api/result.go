package api

import (
	"strconv"

	b "bitbucket.org/devgre/bybit-go"
	"bitbucket.org/devgre/bybit-go/utils"
)

type KLine struct {
	StartTime int64   `json:"start_time"`
	Open      float64 `json:"open"`
	High      float64 `json:"high"`
	Low       float64 `json:"low"`
	Close     float64 `json:"close"`
	Volume    float64 `json:"volume"`
	EndTime   int64   `json:"end_time"`
}

type GetKlineResult struct {
	b.BaseResult
	Result [][]interface{} `json:"result"`
}

func (r *GetKlineResult) getKLineData() (result []KLine) {
	for _, res := range r.Result {
		kline := KLine{}
		kline.StartTime = utils.ConvertScientificNotationToInt64(res[0].(float64))
		kline.Open, _ = strconv.ParseFloat(res[1].(string), 64)
		kline.High, _ = strconv.ParseFloat(res[2].(string), 64)
		kline.Low, _ = strconv.ParseFloat(res[3].(string), 64)
		kline.Close, _ = strconv.ParseFloat(res[4].(string), 64)
		kline.Volume, _ = strconv.ParseFloat(res[5].(string), 64)
		kline.EndTime = utils.ConvertScientificNotationToInt64(res[6].(float64))
		result = append(result, kline)
	}
	return
}

type Order struct {
	AccountId    string `json:"accountId"`
	OrderId      string `json:"orderId"`
	OrderLinkId  string `json:"orderLinkId"`
	Symbol       string `json:"symbol"`
	TransactTime string `json:"transactTime"`
	Price        string `json:"price"`
	OrigQty      string `json:"origQty"`
	ExecutedQty  string `json:"executedQty"`
	Type         string `json:"type"`   // Order type LIMIT MARKET LIMIT_MAKER
	Side         string `json:"side"`   // Order direction BUY SELL
	Status       string `json:"status"` // Order status
	TimeInForce  string `json:"timeInForce"`
	ExecQty      string `json:"execQty"`
}

type PlaceOrderResult struct {
	b.BaseResult
	Result Order `json:"result"`
}

type OrderInfo struct {
	AccountId           string `json:"accountId"`           //  "10054",
	ExchangeId          string `json:"exchangeId"`          //  "301",
	Symbol              string `json:"symbol"`              //  "ETHUSDT",
	SymbolName          string `json:"symbolName"`          //  "ETHUSDT",
	OrderLinkId         string `json:"orderLinkId"`         //  "162080709527252",
	OrderId             string `json:"orderId"`             //  "889788838461927936",
	Price               string `json:"price"`               //  "20000",
	OrigQty             string `json:"origQty"`             //  "10",
	ExecutedQty         string `json:"executedQty"`         //  "0",
	CummulativeQuoteQty string `json:"cummulativeQuoteQty"` //  "0",
	AvgPrice            string `json:"avgPrice"`            //  "0",
	Status              string `json:"status"`              //  "NEW",
	TimeInForce         string `json:"timeInForce"`         //  "GTC",
	Type                string `json:"type"`                //  "LIMIT",
	Side                string `json:"side"`                //  "BUY",
	StopPrice           string `json:"stopPrice"`           //  "0.0",
	IcebergQty          string `json:"icebergQty"`          //  "0.0",
	Time                string `json:"time"`                //  "1620807095287",
	UpdateTime          string `json:"updateTime"`          //  "1620807095307",
	IsWorking           bool   `json:"isWorking"`           //  true
}

type OpenOrdersResult struct {
	b.BaseResult
	Result []OrderInfo `json:"result"`
}

type Balance struct {
	Coin     string `json:"coin"`
	CoinId   string `json:"coinId"`
	CoinName string `json:"coinName"`
	Total    string `json:"total"`
	Free     string `json:"free"`
	Locked   string `json:"locked"`
}

type BalanceList struct {
	Items []Balance `json:"balances"`
}

type BalancesResult struct {
	b.BaseResult
	Result BalanceList `json:"result"`
}
