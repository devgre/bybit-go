package api

import (
	"log"
	"testing"
	"time"

	bybit_go "bitbucket.org/devgre/bybit-go"
	"bitbucket.org/devgre/bybit-go/utils"
)

const (
	// Main
	//BaseUrl = "https://api.bybit.com"
	//ApiKey = "1wzxIKLfWjUIZFSVcA"
	//SecretKey = "DxXI8cqWozzGtshPkhPm8iqtVmhQfJDDjBYY"

	// Test
	BaseUrl   = bybit_go.TestnetEndpoint
	ApiKey    = "bLVooDimjXst3TpibW"
	SecretKey = "cCTjLc8AoLrg9wlJqxyqtrbOdEikY0uZc35X"
)

func newByBit() *ByBitSpot {
	baseURL := BaseUrl
	apiKey := ApiKey
	secretKey := SecretKey
	b := NewSpotApi(nil, baseURL, apiKey, secretKey, true)
	err := b.SetCorrectServerTime()
	if err != nil {
		log.Printf("%v", err)
	}
	return b
}

func TestByBitSpot_GetServerTime(t *testing.T) {
	b := newByBit()
	ms, err := b.GetServerTime()
	if err != nil {
		t.Error("Spot.GetServerTime failed: ", err)
	}
	t.Log(ms, utils.TimeFromUnixTime(ms))
}

func TestByBitSpot_GetKLine(t *testing.T) {
	b := newByBit()
	startTime := time.Now().Add(-1*time.Hour).Unix() * 1000
	endTime := time.Now().Unix() * 1000
	res, err := b.GetKLine("BTCUSDT", "5m", startTime, endTime, 0)
	if err != nil {
		t.Error("Spot.GetKLine failed: ", err)
	}
	for _, v := range res {
		t.Logf("%#v", v)
	}
}

func TestByBitSpot_PlaceOrder(t *testing.T) {
	b := newByBit()
	qty := 0.001998
	price := 50000.0
	res, err := b.PlaceOrder("BTCUSDT", "Limit", "Sell", "GTC", price, qty)
	if err != nil {
		t.Log("PlaceOrder failed: ", err)
		return
	}

	t.Log(res)
	t.Logf("OrderId: %s. Use it for order cancellation", res.OrderId)
}

func TestByBitSpot_CancelOrder(t *testing.T) {
	b := newByBit()
	res, err := b.CancelOrder("1073423147033839616")
	if err != nil {
		t.Log("CancelOrder failed: ", err)
		return
	}

	t.Log(res)
	t.Logf("OrderId: %s cancelled!", res.OrderId)
}

func TestByBitSpot_CancelOrderFast(t *testing.T) {
	b := newByBit()
	res, err := b.CancelOrderFast("BTCUSDT", "1072726318596002048")
	if err != nil {
		t.Log("CancelOrderFast failed: ", err)
		return
	}

	t.Log(res)
	t.Logf("OrderId: %s cancelled!", res.OrderId)
}

func TestByBitSpot_GetOpenOrders(t *testing.T) {
	b := newByBit()
	res, err := b.GetOpenOrders("BTCUSDT", "", 0)
	if err != nil {
		t.Log("GetOpenOrders failed: ", err)
		return
	}
	t.Logf("Orders: %v", res)
}

func TestByBitSpot_GetWalletBalance(t *testing.T) {
	b := newByBit()
	res, err := b.GetWalletBalance()
	if err != nil {
		t.Log("GetWalletBalance failed: ", err)
		return
	}
	t.Logf("Balance: %v", res)
}
