package api

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	bybit_go "bitbucket.org/devgre/bybit-go"
)

type ByBitSpot struct {
	bybit_go.BaseApi
}

func NewSpotApi(httpClient *http.Client, baseURL string, apiKey string, secretKey string, debugMode bool) *ByBitSpot {
	if httpClient == nil {
		httpClient = &http.Client{
			Timeout: 10 * time.Second,
		}
	}
	return &ByBitSpot{bybit_go.BaseApi{
		BaseURL:   baseURL,
		ApiKey:    apiKey,
		SecretKey: secretKey,
		Client:    httpClient,
		DebugMode: debugMode,
	}}
}

func (b *ByBitSpot) SetCorrectServerTime() (err error) {
	var timeNow int64
	timeNow, err = b.GetServerTime()
	if err != nil {
		return
	}
	b.ServerTimeOffset = timeNow - time.Now().UnixNano()/1e6
	return
}

// GetServerTime
// https://bybit-exchange.github.io/docs/spot/?console#t-servertime
// timeNow - ms
func (b *ByBitSpot) GetServerTime() (timeNow int64, err error) {
	params := map[string]interface{}{}
	var ret bybit_go.GetServerTimeResult
	if err = b.Request(http.MethodGet, "/spot/v1/time", params, &ret); err != nil {
		return
	}
	timeNow = ret.Result.Value
	return
}

// GetKLine
// https://bybit-exchange.github.io/docs/spot/#t-querykline
// interval: 1 3 5 15 30 60 120 240 360 720 "D" "M" "W" "Y"
// startTime: Start time, unit in millisecond
// endTime: End time, unit in millisecond
// limit: Default value is 1000, max 1000
func (b *ByBitSpot) GetKLine(symbol string, interval string, startTime, endTime int64, limit int) (result []KLine, err error) {
	params := map[string]interface{}{}
	params["symbol"] = symbol
	params["interval"] = interval
	params["startTime"] = startTime
	params["endTime"] = endTime
	if limit > 0 {
		params["limit"] = limit
	}

	var ret GetKlineResult
	err = b.Request(http.MethodGet, "/spot/quote/v1/kline", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	result = ret.getKLineData()
	return
}

// PlaceOrder
// https://bybit-exchange.github.io/docs/spot/?console#t-placeactive
func (b *ByBitSpot) PlaceOrder(symbol string, orderType string, orderSide string, timeInForce string, price, qty float64) (order Order, err error) {
	params := map[string]interface{}{}
	params["symbol"] = symbol
	params["type"] = orderType
	params["side"] = orderSide
	params["timeInForce"] = timeInForce
	params["qty"] = qty
	if price > 0 {
		params["price"] = price
	}

	var ret PlaceOrderResult
	err = b.SignedRequest(http.MethodPost, "/spot/v1/order", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	order = ret.Result
	return
}

// CancelOrder
// https://bybit-exchange.github.io/docs/spot/?console#t-cancelactive
func (b *ByBitSpot) CancelOrder(orderId string) (order Order, err error) {
	params := map[string]interface{}{
		"orderId": orderId,
	}

	var ret PlaceOrderResult
	err = b.SignedRequest(http.MethodDelete, "/spot/v1/order", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	order = ret.Result
	return
}

// CancelOrderFast
// https://bybit-exchange.github.io/docs/spot/?python--pybit#t-fastcancelactiveorder
func (b *ByBitSpot) CancelOrderFast(symbol string, orderId string) (order Order, err error) {
	params := map[string]interface{}{
		"symbolId": symbol,
		"orderId":  orderId,
	}

	var ret PlaceOrderResult
	err = b.SignedRequest(http.MethodDelete, "/spot/v1/order/fast", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	order = ret.Result
	return
}

// BatchCancelOrders
// https://bybit-exchange.github.io/docs/spot/?console#t-batchcancelactiveorder
func (b *ByBitSpot) BatchCancelOrders(symbol, side, orderTypes string) (ok bool, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}
	if len(side) > 0 {
		params["side"] = side
	}
	if len(orderTypes) > 0 {
		params["orderTypes"] = orderTypes
	}

	var ret bybit_go.BaseResult
	err = b.SignedRequest(http.MethodDelete, "/spot/order/batch-cancel", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	ok = true
	return
}

// BatchCancelOrdersFast
// https://bybit-exchange.github.io/docs/spot/?console#t-batchfastcancelactiveorder
func (b *ByBitSpot) BatchCancelOrdersFast(symbol, side, orderTypes string) (ok bool, err error) {
	params := map[string]interface{}{
		"symbol": symbol,
	}
	if len(side) > 0 {
		params["side"] = side
	}
	if len(orderTypes) > 0 {
		params["orderTypes"] = orderTypes
	}

	var ret bybit_go.BaseResult
	err = b.SignedRequest(http.MethodDelete, "/spot/order/batch-fast-cancel", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	ok = true
	return
}

// GetOpenOrders
// https://bybit-exchange.github.io/docs/spot/?python--pybit#t-openorders
func (b *ByBitSpot) GetOpenOrders(symbol string, orderId string, limit int) (orders []OrderInfo, err error) {
	params := map[string]interface{}{}
	if len(symbol) > 0 {
		params["symbol"] = symbol
	}
	if len(orderId) > 0 {
		params["orderId"] = orderId
	}
	if limit > 0 {
		params["limit"] = limit
	}

	var ret OpenOrdersResult
	err = b.SignedRequest(http.MethodGet, "/spot/v1/open-orders", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	orders = ret.Result
	return
}

// GetWalletBalance
// https://bybit-exchange.github.io/docs/spot/?console#t-wallet
func (b *ByBitSpot) GetWalletBalance() (balance BalanceList, err error) {
	params := map[string]interface{}{}

	var ret BalancesResult
	err = b.SignedRequest(http.MethodGet, "/spot/v1/account", params, &ret)
	if err != nil {
		return
	}

	if ret.RetCode != 0 {
		err = errors.New(fmt.Sprintf("%d: %s", ret.RetCode, ret.RetMsg))
		return
	}

	balance = ret.Result
	return
}
