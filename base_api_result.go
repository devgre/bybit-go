package bybit_go

type BaseResult struct {
	RetCode int    `json:"ret_code"`
	RetMsg  string `json:"ret_msg"`
	ExtCode string `json:"ext_code"`
	ExtInfo string `json:"ext_info"`
	TimeNow string `json:"time_now"`
}

type ServerTime struct {
	Value int64 `json:"serverTime"`
}

type GetServerTimeResult struct {
	BaseResult
	Result ServerTime `json:"result"`
}
