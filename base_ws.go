package bybit_go

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"bitbucket.org/devgre/bybit-go/recws"

	"github.com/chuckpreslar/emission"
	"github.com/gorilla/websocket"
)

type Configuration struct {
	Addr      string `json:"addr"`
	ApiKey    string `json:"api_key"`
	SecretKey string `json:"secret_key"`
	DebugMode bool   `json:"debug_mode"`
}

type BaseWS struct {
	ctx    context.Context
	cancel context.CancelFunc
	conn   *recws.RecConn
	mu     sync.RWMutex

	emitter *emission.Emitter

	Cfg           *Configuration
	subscribeCmds []string
}

func NewBaseWS(cfg *Configuration) *BaseWS {
	b := &BaseWS{
		Cfg:     cfg,
		emitter: emission.NewEmitter(),
	}
	b.ctx, b.cancel = context.WithCancel(context.Background())

	b.conn = &recws.RecConn{
		KeepAliveTimeout: 30 * time.Second,
		LogVerbose:       true,
	}
	//if cfg.Proxy != "" {
	//	proxy, err := url.Parse(cfg.Proxy)
	//	if err != nil {
	//		return nil
	//	}
	//	b.conn.Proxy = http.ProxyURL(proxy)
	//}
	b.conn.SubscribeHandler = b.subscribeHandler
	return b
}

func (b *BaseWS) subscribeHandler() error {
	if b.Cfg.DebugMode {
		log.Printf("ByBitWS subscribeHandler")
	}

	b.mu.Lock()
	defer b.mu.Unlock()

	if b.Cfg.ApiKey != "" && b.Cfg.SecretKey != "" {
		err := b.Auth()
		if err != nil {
			log.Printf("ByBitWS auth error: %v", err)
		}
	}

	for _, cmd := range b.subscribeCmds {
		err := b.send(cmd)
		if err != nil {
			log.Printf("ByBitWS re-subscribe error: %v", err)
		}
	}

	return nil
}

func (b *BaseWS) closeHandler(code int, text string) error {
	log.Printf("ByBitWS closeHandle executed code=%v text=%v", code, text)
	return nil
}

func (b *BaseWS) IsConnected() bool {
	return b.conn.IsConnected()
}

func (b *BaseWS) Sub(cmdStr string) error {
	b.subscribeCmds = append(b.subscribeCmds, cmdStr)
	return b.send(cmdStr)
}

func (b *BaseWS) Unsub(cmdStr string) error {
	var id = -1
	for i, cmd := range b.subscribeCmds {
		if cmd == cmdStr {
			id = i
			break
		}
	}
	if id >= 0 {
		b.subscribeCmds = append(b.subscribeCmds[:id], b.subscribeCmds[id+1:]...)
	}
	return b.send(cmdStr)
}

func (b *BaseWS) Start(pingFn func(), processMsgFn func(messageType int, data []byte)) error {
	b.conn.Dial(b.Cfg.Addr, nil)

	cancel := make(chan struct{})

	go func() {
		t := time.NewTicker(time.Second * 5)
		defer t.Stop()
		for {
			select {
			case <-t.C:
				if pingFn != nil {
					pingFn()
					return
				}
				b.ping()
			case <-cancel:
				return
			}
		}
	}()

	go func() {
		defer close(cancel)
		for {
			select {
			case <-b.ctx.Done():
				log.Println("ByBitWS Done!")
				go b.conn.Close()
				return
			default:
				if !b.IsConnected() {
					continue
				}

				messageType, data, err := b.conn.ReadMessage()
				if err != nil {
					log.Printf("ByBitWS Read error: %v", err)
					continue
				}
				processMsgFn(messageType, data)
			}
		}
	}()

	return nil
}

func (b *BaseWS) Stop() {
	b.cancel()
}

func (b *BaseWS) Auth() error {
	expires := time.Now().Unix()*1000 + 10000 // ms
	req := fmt.Sprintf("GET/realtime%d", expires)
	sig := hmac.New(sha256.New, []byte(b.Cfg.SecretKey))
	sig.Write([]byte(req))
	signature := hex.EncodeToString(sig.Sum(nil))

	cmdStr := fmt.Sprintf(`{"op":"auth","args":["%s",%d,"%s"]}`, b.Cfg.ApiKey, expires, signature)
	return b.send(cmdStr)
}

func (b *BaseWS) ping() {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("ByBitWS ping error: %v", r)
		}
	}()

	if !b.IsConnected() {
		return
	}
	err := b.send(`{"op":"ping"}`)
	if err != nil {
		log.Printf("ByBitWS ping error: %v", err)
	}
}

func (b *BaseWS) HandlePong() (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.New(fmt.Sprintf("ByBitWS handlePong error: %v", r))
		}
	}()
	pongHandler := b.conn.PongHandler()
	if pongHandler != nil {
		err = pongHandler("pong")
	}
	return
}

func (b *BaseWS) SendCmd(cmd interface{}) error {
	data, err := json.Marshal(cmd)
	if err != nil {
		return err
	}
	return b.send(string(data))
}

func (b *BaseWS) SendBCmd(cmdBytes []byte) error {
	return b.send(string(cmdBytes))
}

func (b *BaseWS) send(msg string) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.New(fmt.Sprintf("ByBitWS send error: %v", r))
		}
	}()

	if b.Cfg.DebugMode {
		log.Printf("ByBitWS send: %s", msg)
	}
	err = b.conn.WriteMessage(websocket.TextMessage, []byte(msg))
	return
}
