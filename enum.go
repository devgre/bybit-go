package bybit_go

type Period int

const (
	Period1m Period = iota
	Period5m
	Period15m
	Period30m
	Period1h
	Period4h
	Period12h
	Period1d
	Period1w
	Period1M
)

func (i Period) toPerpetual() string {
	switch {
	case i == Period1m:
		return "1"
	case i == Period5m:
		return "5"
	case i == Period15m:
		return "15"
	case i == Period30m:
		return "30"
	case i == Period1h:
		return "60"
	case i == Period4h:
		return "240"
	case i == Period12h:
		return ""
	case i == Period1d:
		return "D"
	case i == Period1w:
		return "W"
	case i == Period1M:
		return "M"
	default:
		return ""
	}
}
func (i Period) toSpot() string {
	switch {
	case i == Period1m:
		return "1m"
	case i == Period5m:
		return "5m"
	case i == Period15m:
		return "15m"
	case i == Period30m:
		return "30m"
	case i == Period1h:
		return "1h"
	case i == Period4h:
		return "4h"
	case i == Period12h:
		return "12h"
	case i == Period1d:
		return "1d"
	case i == Period1w:
		return "1w"
	case i == Period1M:
		return "1M"
	default:
		return ""
	}
}
