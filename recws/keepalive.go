package recws

import (
	"sync"
	"time"
)

type KeepAliveRsp struct {
	lastResponse time.Time
	sync.RWMutex
}

func (k *KeepAliveRsp) setLastResponse() {
	k.Lock()
	defer k.Unlock()

	k.lastResponse = time.Now()
}

func (k *KeepAliveRsp) getLastResponse() time.Time {
	k.RLock()
	defer k.RUnlock()

	return k.lastResponse
}
