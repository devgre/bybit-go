package utils

import (
	"fmt"
	"math/big"
	"time"
)

func ConvertScientificNotationToInt64(input float64) int64 {
	f, _, err := big.ParseFloat(fmt.Sprintf("%f", input), 10, 0, big.ToNearestEven)
	if err != nil {
		return 0
	}

	var i = new(big.Int)
	i, _ = f.Int(i)
	return i.Int64()
}

func TimeFromUnixTime(ms int64) time.Time {
	return time.Unix(0, ms*int64(time.Millisecond))
}

func Contains(slice []string, value string) bool {
	for _, val := range slice {
		if val == value {
			return true
		}
	}
	return false
}