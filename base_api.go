package bybit_go

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/json-iterator/go"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"
)

const (
	TestnetEndpoint = "https://api-testnet.bybit.com"

	MainEndpoint  = "https://api.bybit.com"
	MainEndpoint2 = "https://api.bytick.com"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

type BaseApi struct {
	BaseURL          string
	ApiKey           string
	SecretKey        string
	ServerTimeOffset int64
	Client           *http.Client
	DebugMode        bool
}

func (b *BaseApi) Request(method string, apiURL string, params map[string]interface{}, result interface{}) (err error) {
	var keys []string
	for k := range params {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var p []string
	for _, k := range keys {
		p = append(p, fmt.Sprintf("%v=%v", k, params[k]))
	}

	param := strings.Join(p, "&")
	fullURL := b.BaseURL + apiURL
	if param != "" {
		fullURL += "?" + param
	}
	if b.DebugMode {
		log.Printf("Request: %v", fullURL)
	}
	var binBody = bytes.NewReader(make([]byte, 0))

	err = b.call(method, fullURL, binBody, result)
	return
}

func (b *BaseApi) SignedRequest(method string, apiURL string, params map[string]interface{}, result interface{}) (err error) {
	timestamp := time.Now().UnixNano()/1e6 + b.ServerTimeOffset

	params["api_key"] = b.ApiKey
	params["timestamp"] = timestamp

	var keys []string
	for k := range params {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var p []string
	for _, k := range keys {
		p = append(p, fmt.Sprintf("%v=%v", k, params[k]))
	}

	param := strings.Join(p, "&")
	signature := b.getSigned(param)
	param += "&sign=" + signature

	fullURL := b.BaseURL + apiURL + "?" + param
	if b.DebugMode {
		log.Printf("SignedRequest: %v", fullURL)
	}
	var binBody = bytes.NewReader(make([]byte, 0))

	err = b.call(method, fullURL, binBody, result)
	return
}

func (b *BaseApi) getSigned(param string) string {
	sig := hmac.New(sha256.New, []byte(b.SecretKey))
	sig.Write([]byte(param))
	signature := hex.EncodeToString(sig.Sum(nil))
	return signature
}

func (b *BaseApi) call(method, url string, body io.Reader, result interface{}) (err error) {
	var request *http.Request
	request, err = http.NewRequest(method, url, body)
	if err != nil {
		return
	}

	var response *http.Response
	response, err = b.Client.Do(request)
	if err != nil {
		return
	}
	defer response.Body.Close()

	var rspBytes []byte
	rspBytes, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	if b.DebugMode {
		log.Printf("%v", string(rspBytes))
	}

	err = json.Unmarshal(rspBytes, result)
	return
}
